#!/bin/bash

# Sourcing path to Vivado 
source /usr/local/Vivado-2017.4/Vivado/2017.4/settings64.sh 
# Sourcing path to SDK
source /usr/local/Vivado-2017.4/SDK/2017.4/settings64.sh 

# launching SDK 
xsdk -workspace ./Zybo_PTS_Application

