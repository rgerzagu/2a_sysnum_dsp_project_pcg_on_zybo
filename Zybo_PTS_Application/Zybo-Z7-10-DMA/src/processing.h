/*
 * processing.h
 *
 *  Created on: 11 janv. 2021
 *      Author: robin
 */

#ifndef SRC_PROCESSING_H_
#define SRC_PROCESSING_H_

#include "./kissFFT/kiss_fft.h"
#include <stdio.h>
#include <math.h>
#include "timers/timers.h"
#include "printers/printers.h"
#include "data_file.h"


/* Mathematical macros */
#define M_PI       3.14159265358979323846
#define SFFT 1024

// Main processing calls 
int processing();
void benchmark();

// Define here all the other prototypes 

#endif /* SRC_PROCESSING_H_ */
