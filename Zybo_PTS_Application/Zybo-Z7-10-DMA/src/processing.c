// Processing functions
#include "processing.h"
#include "kissFFT/kiss_fft.h"
#include <unistd.h>

void benchmark(){
    int mc_runs = 50;
    struct_timer timer;
    /*--------------------------------------------------------------------------
     * --- Main benchmark call (Monte carlo)
     * --------------------------------------------------------------------------*/
    printf("Starting routine\n");
    timer = tic();
    float ppm_mean = 0;
    int ppm_tmp;
    for (int c = 0 ; c < mc_runs ; c++){
        ppm_tmp = processing();
        ppm_mean += (float) ppm_tmp;
    }
    printf("output\n");
    timer = toc(timer);
    print_toc(timer);
    float median_time = timer.duration / (float) mc_runs;
    float median_ppm  = ppm_mean / (float) mc_runs; 
    printf("The medium time per iteration is %2.4f \n",median_time);
    printf("Calculated PPM is  %2.4f \n",median_ppm);
}


int processing()
{
    int bpm;
    return 0
}


